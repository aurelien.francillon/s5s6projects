Beagle Boards
-------------

The classic [BeagleBone Black](https://beagleboard.org/black/) is based on the Texas Instruments (TI) [AM3358 Sitara processor](https://www.ti.com/product/AM3358) that embeds a ARM Cortex-A8 32-bits single core at 1GHz with its NEON SIMD Media Processing Unit, a 2D/3D POWERVR SGX530 graphics accelerator, 512MB of DDR3 memory, 4GB of on-board flash storage, plus two 32-bits RISC cores for real-time applications.
It should be available by the end of May from [Mouser] (unit price 49.69 euros, board only), or in August from [Farnell] (44.75 euros, board plus USB cable).
We would probably need to add a case, cables, a SD card and a power adapter so the total unit price would probably be around 75 euros (added value taxes and shipping excluded).

The ["industrial" version of the BeagleBone Black](https://beagleboard.org/black-industrial), hardened with extended temperature range, is slightly more expensive (about 65-70 euros, about 90-100 euros with accessories) but available now from [Farnell] and should be available from [Mouser] in June.

The [BeagleBone AI](https://beagleboard.org/ai) is based on the TI [AM5729 Sitara processor](https://www.ti.com/product/AM5729?keyMatch=AM5729&tisearch=search-everything&usecase=GPN).
Its CPU is a 1.5 GHz dual core ARM Cortex-A15 (32 bits) processor but it also embeds two floating-point DSPs (TI C66, VLIW), 4 "Embedded Vision Engines" for OpenCL hardware acceleration, a dual core GPU (PowerVR SGX544)...
Its unit price (board only) is 123.75 euros and seems available from [Mouser].
That is about 150 euros with accessories.

- For OS, all fine. All Beagle seem to support the TI RTOs, which does not seem to be open-source, but which could lead to interesting experiments around the project. We could also use Linux Debian that is less real time but offers more ways to improve drivers or do interesting things in the kernel.


Raspberry Pi
------------

In the second family the [Raspberry Pi 4](https://thepihut.com/collections/raspberry-pi/products/raspberry-pi-4-model-b) seems a good candidate (1.5GHz 64-bit quad-core Cortex-A72 ARM CPU, Broadcom VideoCore VI GPU...)
Unit price from the [PiHut]: from 33.70 euros for the bare board with 1GB memory to 73.34 euros with 8GB memory.

[Starter kit](https://thepihut.com/collections/raspberry-pi-kits-and-bundles/products/raspberry-pi-starter-kit?variant=41173475819715) with power adapter, cables, micro-SD card and official case: from 57.48 (1GB) to 97.13 (8GB) euros.

The board and the starter kit are apparently sold out everywhere.

[Mouser]: https://www.mouser.fr/
[Farnell]: https://fr.farnell.com/
[PiHut]: https://thepihut.com/

- For OS, this is fine, with the support of Debian (open-source) but RTOS are not well supported (e.g. xenomai). If no hard real-time tasks are necessary, Linux Debian should be ok.





<!-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 expandtab textwidth=0: -->
