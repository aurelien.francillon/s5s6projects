# S5S6Projects

Preparation work for the new S5 & S6 student projects

## S5 courses

- IntroNet1 (S5) and IntroNet2 (S6)
  * Introduction to networking and the internet protocols embracing a top-down approach. 
  * IntroNet1:
    * Application protocols: HTTP, SMTP, FTP, DNS, Socket, TLS 
    * Transport protocols: TCP, UDP
  * IntroNet2:
    * Internet Protocols: IP and Routing.
    * Link layer protocols (MAC layer): Ethernet, Wifi.  
- IntroArchi
  * Introduction à l'architecture des ordinateurs (Introduction to computer architecture), S5, Renaud Pacalet, 21 hours.
  * The project has a software design and coding part in which a sub-part requires low-level assembly programming, either to access a resource (input-output, peripheral, processor internals...) that cannot be accessed from higher levels, or for pure performance reasons. Any target platform that can be programmed in assembly would probably be OK.


- CompProg
  * Programming Languages and Algorithm Design, S5, Chiara Galdi, 50 hours.
  * The project includes the use of an algorithm for one of the following tasks: sorting, searching, resource optimization, object detection, etc. Students must provide the analysis of the complexity of the algorithm.


- BasicOS
  * Introduction to Operating Systems, S5, Ludovic Apvrille, 21 hours
  * The project includes developping an application running closely with an Operating System (e.g., using syscalls directly, without the LibC), and adding new basic modules to the Operating Systems. Any target platform that supports a version of GNU/Linux >= 4 is fine for this.

- ComLab
  * UE 1e-S5-SIGN :Traitement du signal appliqué, Florian Kaltenbenberger, 27h
  * In this class we take an experimental approach to wireless digital communication. We will use a well-known software defined radio platform known as the USRP (universal software radio peripheral) to implement a simple digital wireless communication link.

- SoundProc
  * UE 1e-S5-SIGN : Traitement son et musique, Massimiliano TODISCO, 27h
  * The project covers the development of algorithms for the analysis, processing and synthesis of audio and music signals.
Students will become experienced with languages dedicated to digital audio processing, such as Pure Data or Simulink. Raspberry or Arduino can be used as a board for this purpose.

- ImProc
  * UE 1e-S5-SIGN : Traitement d'images, Jean-Luc DUGELAY, 27h
  * This class is composed of half and half lectures and laboratories (but no project, no challenge) During session laboratories, students get familiar with basic image processing libraries (e.g. OpenCv) and implement basic digital image processing tools in Python and/or Matlab such as noise filtering, edge detection.


- T3E (Transition environnementale et éthique de l’entreprise)
  * UE 1e-S5-ENJ : Enjeux Stratégiques, environnementaux et éthiques, Laura Draetta, 21h
  * Societal sustainability: identify, assess and improve the environmental, social and/or ethical dimensions of the project. Two possible options to incrementally address the societal sustainability of the project, to be chosen according to the project:
   1. Identify the main environmental, social, and/or ethical dimensions of the project; draw up an analysis of the project in terms of strengths/weaknesses in relation to these dimensions; 
   2. Identify the main environmental, social, and/or ethical dimensions of the project; draw up an analysis of the project in terms of strengths/weaknesses in relation to these dimensions; propose an approach to improve one of these dimensions

## S6 courses

- IntroNet2

- IntroSec
  * Introduction To Cybersecurity, S6, Simone Aonzo, 21 hours.
  * The project consists of using Docker to virtualize a network with multiple hosts and create a cyber range, a controlled environment where cybersecurity professionals learn how to detect and mitigate cyber-attacks using the same equipment they will have in the real world. Finally, the student must act as a Red Team operator, breaching the security measures and providing a report on the vulnerabilities found. Any device that supports a version of GNU/Linux to run Docker is fine (even a Raspberry Pi >= 4).

- IntroDB
  * Introduction to Data Bases, S6, Pasquale Lisena, 21 hours
  * The project consist in realising a NoSQL database for a specific domain, crawling and harmonising data from different sources, and choosing the easier data structure for the specific problems. The required platform would need to be compatible with MongoDB, Neo4j and Python. Memory can be a limitation to the choice of the platform, so I think the computers of the lab would be better.

- IntroSTATS
  * Foundations of Statistical Inference, S6, Motonobu Kanagawa, 21 hours
  * The project studies and develops a generic statistical approach to the calibration of a computer simulator. It makes use of both frequentist and Bayesian statistical methods, the foundations of which are taught during the course. Any platform is fine with the project.     
  
- InfoTheo
  * InfoTheo -Théorie de l'information – Derya Malak S6, 42 hours
  * In this project, the students will develop a background on the theoretical limits of communications and computation in networks. They will take a deeper look at the problems of source compression, rate-distortion and quantization, and source/channel coding, and distributed source compression to understand the fundamental limits.
  * The project has applications to task-oriented communication and learning when communication networks are tailored for performing specific tasks, understanding fundamental coding rate limits for communication/computation, decentralized and federated learning, and sensing and connected robotics. The students can use any relevant platform, e.g., Matlab, python, for programming.


## Project proposals

Main criteria to consider:

- Target platform price (ideally low enough to donate it to each student).
- Feasibility (100 hours, 4 or 5 students per team).
- Exciting, students motivation is a key factor.
- Possibility to include a kind of competition between the teams (higher motivation, lower temptation to just copy what the others do and to let others copy).
- Cooperation between several devices (as each student will have their platform, it would be great if they were cooperating). Inter-team cooperation (which does not exclude a form of competition) is also an option.
- Possibility of extension to semester S6 and its courses.

### Collaborative exploration

**Requirements:**

- Raspberry Pi
- Camera
- 4 LEDs

**Goal:**

The goal is to explore an environment in a limited time. The environment is a simple map consisting of connected vertical and horizontal lines. There are some markers attached to the lines to help the map exploration process.

An example of the map with markers is shown in Fig. 1.

![Figure 1: An example of the map to be explored](images/collaborative-exploration-fig1.png)  
_Figure 1: An example of the map to be explored_

In this example, the QR-code is used as the marker to help the map exploration.

**Scenario:**

Each person carries a Raspberry Pi (RPi) with a camera attached to it which is facing downwards. The person starts from the starting point (shown in Fig. 1). When the camera detects a marker, the algorithm decides which direction to go next. To communicate the direction decided by the algorithm to the person carrying the device, four LEDs are used which are also connected to the RPi. Each LED shows one of the following directions: right, left, straight, turning back. An example of the LEDs connected to the RPi is illustrated in Fig. 2. The person then moves along the line in the direction decided by the algorithm to reach the next marker. This procedure repeats until the map is fully explored. The map is considered fully explored if all the markers are visited (The algorithm knows in advance how many markers are on the map). At the end of the mission, the teams should provide a map of the environment that they explored and the location of the markers on it.

Note, instead of LEDs, an RPi-compatible display can be used.

![Figure 2: LEDs connected to the RPi for showing which direction to be taken by the person](images/collaborative-exploration-fig2.png)  
_Figure 2: LEDs connected to the RPi for showing which direction to be taken by the person_

**Collaborative exploration:**

To accelerate the exploration, multiple persons (each one carrying an RPi with a camera) can collaborate. In this case, the devices communicate with each other to perform a collaborative exploration of the map. Each device also has a unique id (e.g. a unique QR code attached to the device). This id is used when two persons meet each other (considered as a collision) during the collaborative exploration. In this case, one of the persons scans the other device’s id, and the algorithm should make an appropriate decision based on this collision.

**Extra points:**

There are additional markers on the map. These markers are different than the QR-codes that are used to help explore the environment. If the devices can detect them correctly while exploring the map, they get an extra point for each detected marker. An example of the bonus markers is shown in Fig. 3.

![Figure 3: bonus markers (animal faces) for obtaining extra points during the map exploration](images/collaborative-exploration-fig3.png)  
_Figure 3: bonus markers (animal faces) for obtaining extra points during the map exploration_

### Colours recognition for visually impaired people

- Semester: S5
- Proposer: Renaud
- Description: Attach a camera to the platform, and use it to deliver colour-oriented audio description of scenes ("large dark red square patch in the bottom left, white diagonal stripe from top left to bottom right"...).
- Involved courses/skills/knowledge:
  * BasicOS: modify the camera device driver to adapt it to the project
  * IntroArchi: optimize the camera device driver with some assembly coding of performance-critical sections
  * SoundProc: speech synthesis and speech recognition (to command the device with voice)
  * ImProc: image processing (colours, edges, shapes...)
  * ComProg: algorithms selection, complexity evaluation, programming
  * T3E: analysis of environmental, social and ethical dimensions, suggestions of improvement, evaluation plans...
- Competition possible in the form of a poster presentation where solutions can be tested on, e.g., clothes description and graded by an "independent" jury
- Target platform: Raspberry Pi 4 with camera

### Audio support for navigating an obstacle course for visually impaired people
 
- Semester: S5
- Proposer: Chiara and Max
- Description: Navigating an obstacle course blindfolded, guided by audio feedback from video analysis of the scene. The raspberry pi is connected to a camera and stereo headphones. The device is worn around the neck and captures the scene via the camera. The algorithm developed by the students has to detect the perimeter of the path and obstacles and guide the navigation of the blind person. The video input is transformed into audio feedback in the headphones to indicate the direction in which the person should walk. Students are free to choose the most appropriate algorithm for this purpose.
- Involved courses/skills/knowledge:
  * BasicOS: modify the camera device driver to adapt it to the project
  * IntroArchi: optimize the camera device driver with some assembly coding of performance-critical sections
  * SoundProc: a stereo audio signal is used to direct the person through the obstacle course. Directionality exploits the spatial perception of our auditory system. The audio signal might have a higher frequency component or a higher loudness on the side towards which the person has to turn. More refined techniques such as binaural auralisation could be used. In this case, the direction is encoded as a synthesised acoustic event and placed in three-dimensional space. 
  * ImProc: image processing to detect the perimeter of the course and the obstacles (e.g. cardboard boxes with markers, perimeter indicated by markers)
  * ComProg: navigation algorithms, complexity evaluation, programming
  * T3E: analysis of environmental, social and ethical dimensions, suggestions of improvement, evaluation plans...
- Competition: Finish the course in as little time as possible and with as few mistakes as possible (hitting an obstacle corresponds to a penalty).
- Target platform: Raspberry Pi 4, camera, stereo headphones, battery
- Programming language(s): Simulink, MATLAB

### Audiovisual art
 
- Semester: S5
- Proposer: Max and Chiara
- Description: 
* Option 1) Hand proximity recognition with proximity sensor to change volume (e.g. low volume to high volume). Hand gesture recognition using a camera, N classes = different gestures (determine sound characteristics, e.g. pitch and timbre). 
* Option 2) There is a canvas (e.g. whiteboard) where adding objects or symbols changes the sound characteristics, e.g. pitch, timbre and loudness, and the colour and intensity of the lights.
- Involved courses/skills/knowledge:
  * BasicOS: modify the camera device driver to adapt it to the project
  * IntroArchi: optimize the camera device driver with some assembly coding of performance-critical sections
  * SoundProc: Option 1: the sound changes according to the proximity of one hand and the gesture of the other hand; Option2: placing symbols or objects on the canvas corresponds to selecting different sounds. 
  * ImProc: Option 1: image processing to detect and recognise the hand gesture; Option 2: image processing to detect and recognize the symbols or objects on the canvas and their position on the canvas.
  * ComProg: matching algorithm, complexity evaluation, programming
  * T3E: analysis of environmental, social and ethical dimensions, suggestions of improvement, evaluation plans...
- Evaluation: subjective assessment by colleagues and professors (voting).
- Target platform: Raspberry Pi 4, camera, speakers, proximity sensors (for option1), led lights (for option 2)
- Programming languages: Simulink, MATLAB

### Turn Game
 
- Semester: S5
- Proposer: Pasquale
- Description: Realise a game in which 2 different devices (the chosen platform) play one against the other in turn. The game rules can be chosen in order to maxise the number of involved skills. E.g. Tetris (in turn, collaborative or competitive) or Pong, driven by hand gestures. E.g.2 Pictionary, the answer require character/speech recognition
- Involved courses/skills/knowledge:
  * BasicOS: modify the camera device driver to adapt it to the project
  * IntroArchi: optimize the camera device driver with some assembly coding of performance-critical sections
  * SoundProc: the sound changes according on how in danger/close to the solution you are in the game
  * ImProc:  image processing to detect and recognise the hand gesture
  * ComProg: matching algorithm, programming
  * IntroNet1: communication between the 2 devices
- Evaluation: subjective assessment by colleagues and professors (voting).
- Target platform: Raspberry Pi 4, camera, speakers, eventually mic



### Consensus algorithm for image classification using a mesh network
 
- Semester: S5
- Proposer: Simone
- Vision statement: United we stand, divided we fall
- Description: in this project, students must configure their devices in order to create a mesh network. Then, they have to agree on a protocol to communicate in broadcast to which category belongs the animal (for example) that they detect through the webcam according to their image processing program. Then, using a consensus algorithm, decide "definitively" to which category it belongs. In this project we assume that all participants are cooperative and there are no malicious actors. The logs must be in a predetermined format for evaluation. 
- Involved S5 courses/skills/knowledge:
  * BasicOS and ComLab: config of the mesh network
  * ImProc: image processing to detect and classify the animal
  * ComProg: matching algorithm, programming
  * IntroNet1: communication protocol
- Evaluation: We decide the "evaluation day" and we display some images on a big screen. Then, given a metric (like F1 score), each group will be evaluated on both the image classification of their image processing program, and the classification of their consensus algorithm.
- Target platform: Raspberry Pi 4 and camera
- Extension to semester S6: We assume the presence of malicious actors (however, the Denial-Of-Service is not among the capabilities of the attacker model). Students need to improve their communication protocol by making it *secure* through cryptographic primitives; namely, they must guarantee confidentiality, integrity, and authenticity of each message -- not availability since we excluded Denial-Of-Service attacks. Moreover, we also assume a non-cooperative environment, i.e., they can transmit wrong results trying to hack the consensus algorithm. Each transmitted/received message and the decisions made by the algorithms must be persisted in a database. 
- Involved S6 courses/skills/knowledge:
  * IntroNet2: improved communication protocol
  * IntroSec: basic cryptography
  * IntroDB: data persistence
  * IntroSTATS: improved consensus algorithm


### High Level Idea and Application

- Proposer: Derya

Distributed agents (RPIs) in a wireless network collectively try to achieve a common goal, e.g., reconstructing a very large IMAGE from distributed pixels at a central agent.  

- Each RPI holds a subset of pixels. However, it does not know the information of its coordinates
- Each RPI will be given a data vector, containing the ID and the coordinate information of only a subset of RPIs (local neighborhood). A collection of RPIs might have shared (repeated) information about IDs and the coordinates.
- Each RPI can communicate only with its local neighborhood and exchange information pertaining to the ID and the coordinates.
- There is a central agent that collects the coordinate information from the RPIs. Once the pixel coordinates are collected, it can reconstruct the IMAGE.

_Communication inefficient._ One trivial approach with no cooperation is the following. Each RPI transmits directly to the central agent its vector in its entirety. Since RPIs have redundant data, this is not the most efficient way of reconstructing the IMAGE.

_Storage inefficient._ Another trivial approach could be full cooperation, where each RPI transmits its whole data vector with its neighbors and each RPI learns the IDs and the coordinates of the rest of the agents. Then, it suffices if one designated RPI communicates the coordinates to the central agent.

_Balance._ An improved approach could be to have a constraint on cooperation such that each agent can only gather a restricted amount of information, e.g., 1 bit from each neighbor, and use this information to decide what to transmit to the central agent.

_Your solution._ Is there a better approach that provides a low storage utilization as well as low communication complexity?


Objectives/Questions
- How does cooperation help? More specifically, what is the tradeoff between the number of bits exchanged among the agents and the total number of bits to be communicated to the central agent to reconstruct the IMAGE?
- How does the central agent know whether the constructed IMAGE is accurate? What is a good measure of accuracy?
- Can we incorporate learning into this problem?

<!-- vim: set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab textwidth=0: -->
